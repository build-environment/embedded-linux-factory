# -*- coding: utf-8 -*-

from __future__ import annotations

import importlib
import logging
import pkgutil
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, cast

import click

from . import COMMANDS_PACKAGE_NAME, COMMANDS_PATH, DOCKER_IMAGE
from .elf import Elf
from .libs import click_log
from .libs.docker_env import DockerEnv

_LOG_LEVELS = [
    logging.DEBUG,
    logging.INFO,
    logging.WARNING,
    logging.ERROR,
]


class ElfCli(click.Group):
    def __init__(self, name: Optional[str] = None, **attrs: Any) -> None:
        self.__complete_args: Dict[str, Callable[[click.Context], None]] = {}
        commands: List[click.Command] = []

        for _, module_name, _ in pkgutil.iter_modules(
            [str(COMMANDS_PATH)],
            f"{COMMANDS_PACKAGE_NAME}.",
        ):
            module = importlib.import_module(module_name)
            module_entries = dir(module)

            if "command" in module_entries:
                commands.append(module.command)
            else:
                continue

            if "complete_args" in module_entries:
                self.__complete_args[
                    module.command.name
                ] = module.complete_args

        super().__init__(name, commands, **attrs)

    def complete_args(self, ctx: click.Context):
        if ctx.invoked_subcommand in self.__complete_args:
            self.__complete_args[ctx.invoked_subcommand](ctx)


######################################################################
# ELF entrypoint
######################################################################
@click.group(cls=ElfCli)
@click.version_option(message="%(prog)s v%(version)s")
@click.option(
    "--verbosity",
    "-V",
    type=click.Choice(
        [logging.getLevelName(level) for level in _LOG_LEVELS],
        False,
    ),
    default="INFO",
    show_choices=True,
)
@click.option(
    "--verbose",
    "-v",
    "verbosity",
    is_eager=True,
    is_flag=True,
    flag_value="DEBUG",
    help="Set log level to debug",
)
@click.option(
    "--quiet",
    "-q",
    "verbosity",
    is_eager=True,
    is_flag=True,
    flag_value="ERROR",
    help="Set log level to error",
)
@click.option(
    "--no-color",
    is_flag=True,
    help="Disable colored output.",
)
@click.option(
    "--cwd",
    "-d",
    type=click.Path(
        exists=True, file_okay=False, dir_okay=True, path_type=Path
    ),
)
@click.option(
    "--workspace",
    "-w",
    type=click.Path(
        exists=True, file_okay=False, dir_okay=True, path_type=Path
    ),
    default=None,
)
@click.option(
    "--force-pull",
    is_flag=True,
    help="Force the docker image pull.",
)
@click.help_option("--help", "-h")
@click.pass_context
def cli(
    ctx: click.Context,
    verbosity: str,
    no_color: bool,
    cwd: Optional[Path],
    workspace: Optional[Path],
    force_pull: bool,
):
    if workspace:
        workspace = workspace.absolute()
        if cwd and cwd.absolute().match(f"{str(workspace)}/*"):
            cwd = cwd.absolute()
        elif not cwd and Path.cwd().absolute().match(f"{str(workspace)}/*"):
            cwd = Path.cwd().absolute()
        elif not cwd:
            cwd = workspace
        else:
            click.echo("cwd shall be a sub-directory path of workspace")
            ctx.abort()
    else:
        cwd = (cwd or Path.cwd()).absolute()
        workspace = cwd

    elf = Elf(
        logger=click_log.basic_config(
            log_level=verbosity,
            fmt="[%(levelname)-8s] %(message)s",
            no_fmt_levels=[logging.INFO],
        ),
        cwd=cwd,
        workspace=workspace,
        no_colors=no_color,
        verbose=(verbosity == "DEBUG"),
        docker_env=DockerEnv(
            DOCKER_IMAGE,
            [
                "--proxy",
                "--workdir",
                str(workspace),
                "--cd",
                str(cwd),
            ],
        ).with_volume(Elf.TOOLS_VOLUME),
    )

    if not elf.docker_env.check():
        elf.logger.warning("Docker image not found.")
        force_pull = True

    if force_pull:
        elf.logger.info("Pulling docker image: %s...", elf.docker_env.image)
        elf.docker_env.pull_image()

    ctx.obj = elf

    elf_cli = cast(ElfCli, ctx.command)
    elf_cli.complete_args(ctx)
