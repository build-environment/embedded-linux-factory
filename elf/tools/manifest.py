#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import subprocess
from argparse import ArgumentParser, ArgumentTypeError, Namespace
from pathlib import Path
from typing import Dict, Generator, Iterable, TextIO, Tuple, Union, cast

from lxml import etree


class ArgparseError(Exception):
    pass


def iter_xml(
    file: Union[TextIO, Path], filters: Union[str, Iterable[str]] = "*"
) -> Generator[Tuple[str, Dict[str, str]], None, None]:
    root = etree.parse(file).getroot()
    if root.tag != "manifest":
        return

    if isinstance(filters, str):
        filters = [filters]

    try:
        for xpath_filter in filters:
            root.xpath(xpath_filter)
    except etree.XPathEvalError as exc:
        raise ValueError(
            f"Invalid XPath expression: '{xpath_filter}'"
        ) from exc

    for xpath_filter in filters:
        for child in root.findall(xpath_filter):
            yield cast(str, child.tag), cast(Dict[str, str], child.attrib)


def repo_top_level() -> Path:
    result = subprocess.run(
        ["repo", "--show-toplevel"],
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
        check=True,
    )
    result.check_returncode()

    return Path(result.stdout.decode().strip())


def repo_manifest() -> Path:
    repo_path = repo_top_level() / ".repo"
    manifests_path = repo_path / "manifests"
    manifest_xml = repo_path / "manifest.xml"

    with manifest_xml.open("r", encoding="utf-8") as f:
        _, include_entry = next(iter_xml(f, "./include"))

        return manifests_path.joinpath(include_entry["name"])


def existing_file_path(arg: str) -> Path:
    try:
        file_path = Path(arg)
        assert file_path.exists()
        assert file_path.is_file()
        return file_path
    except Exception as exc:
        raise ArgumentTypeError(
            f"'{arg}' shall match to an existing file path."
        ) from exc


def _manifest_path(args: Namespace):
    manifest_path = repo_manifest()
    if args.absolute:
        print(manifest_path.absolute())
    else:
        print(os.path.relpath(manifest_path, Path.cwd()))


def _manifest_list(args: Namespace):
    try:
        for tag, attrib in iter_xml(args.manifest, args.filters):
            if args.tag:
                if args.tag in attrib:
                    if args.basic_output:
                        print(f"{attrib[args.tag]}")
                    else:
                        print(f"{tag}: '{attrib[args.tag]}'")
                else:
                    continue
            else:
                if args.basic_output:
                    for k, v in attrib.items():
                        print(f"{tag}.{k}='{v}'")
                else:
                    value = " ".join(f"{k}='{v}'" for k, v in attrib.items())
                    print(f"{tag}: {value}")

    except (etree.XMLSyntaxError, ValueError) as exc:
        raise ArgparseError(exc) from exc


def main():
    parser = ArgumentParser()

    try:
        manifest_path = repo_manifest()
    except subprocess.CalledProcessError:
        parser.error(
            "Failed to locate the repo manifest file, "
            "make sure repo is installed and "
            "ensure executing this tool inside a repo project."
        )

    sub_parsers = parser.add_subparsers()
    path_cmd = sub_parsers.add_parser(
        "path", help="Print current manifest path"
    )
    path_cmd.set_defaults(func=_manifest_path)
    path_cmd.add_argument(
        "--absolute",
        "--abs",
        "-a",
        dest="absolute",
        action="store_true",
        help="Print absolute path.",
    )

    list_cmd = sub_parsers.add_parser(
        "list", help="List tag attributes from a manifest file"
    )
    list_cmd.set_defaults(func=_manifest_list)

    list_cmd.add_argument(
        "--manifest",
        help="Manifest file path, default: "
        f"{os.path.relpath(manifest_path, Path.cwd())}",
        default=manifest_path,
        type=existing_file_path,
    )

    list_cmd.add_argument(
        "filters",
        nargs="*",
        type=str,
        help="XPath filters, "
        "see https://www.w3schools.com/xml/xpath_syntax.asp",
    )

    list_cmd.add_argument("--tag", "-t", type=str, help="Select XML tag")

    list_cmd.add_argument(
        "--basic-output",
        "-b",
        action="store_true",
        help="Basic output print, easy to use in bash scripts",
    )

    args = parser.parse_args()
    if "func" not in args:
        parser.print_usage()
        parser.exit(1)
    try:
        args.func(args)
    except ArgparseError as exc:
        parser.error(exc)


if __name__ == "__main__":
    main()
