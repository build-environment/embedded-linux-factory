# -*- coding: utf-8 -*-

import logging
from dataclasses import dataclass
from pathlib import Path
from typing import Generator, Iterable, Optional, Union

from . import TOOLS_PATH
from .libs.docker_env import DockerEnv
from .libs.docker_volume import DockerVolume


@dataclass
class Elf:
    TOOLS_VOLUME = DockerVolume(TOOLS_PATH, "/opt/elf-tools", "ro")
    logger: logging.Logger
    cwd: Path
    workspace: Path
    no_colors: bool
    verbose: bool
    docker_env: DockerEnv

    def manifest_iter(
        self,
        xpath_filters: Union[str, Iterable[str]],
        tag: Optional[str] = None,
    ) -> Generator[str, None, None]:
        cmd = [
            self.TOOLS_VOLUME.joinpath("manifest.py").volume_path,
            "list",
            "--basic-output",
        ]
        if tag:
            cmd += ["--tag", tag]

        if isinstance(xpath_filters, str):
            cmd += [xpath_filters]
        else:
            cmd += list(xpath_filters)

        result = self.docker_env.run(cmd, check=False, capture_output=True)
        if result.returncode:
            return None

        for line in result.stdout.decode().splitlines():
            yield line.strip()
