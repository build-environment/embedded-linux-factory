#!/bin/bash -e

ANSI_COLOR_TS="\033[1;34m"
ANSI_RESET="\033[00m"
echo
echo -e "$ANSI_COLOR_TS   ,(%%%%%%%P°  %%%%%%%%%%%%%%%%%%%%%%),   $ANSI_RESET"
echo -e "$ANSI_COLOR_TS  (%%%%%%°      %%%%%%%%%%%%%%%%%%%%%%%%)  $ANSI_RESET"
echo -e "$ANSI_COLOR_TS  (%%%%%%       %%%%%%%%%%%%%%%%%%%%j%%%)  $ANSI_RESET"
echo -e "$ANSI_COLOR_TS                                     \"²0)  $ANSI_RESET"
echo -e "$ANSI_COLOR_TS                                      ,%)  $ANSI_RESET      .----------------.  .----------------.  .----------------. "
echo -e "$ANSI_COLOR_TS  (%%%%%%%%%%%%%%%%%%%%%%%%%%%%bo,_  ,%%)  $ANSI_RESET     | .--------------. || .--------------. || .--------------. |"
echo -e "$ANSI_COLOR_TS  (%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%,%%%)  $ANSI_RESET     | |  _________   | || |   _____      | || |  _________   | |"
echo -e "$ANSI_COLOR_TS  (%%%%%%            ,%%%%%%%%%%%%%%%%%%)  $ANSI_RESET     | | |_   ___  |  | || |  |_   _|     | || | |_   ___  |  | |"
echo -e "$ANSI_COLOR_TS  (%%%%%%            %%%%%%%%%%%%%%%%%%%)  $ANSI_RESET     | |   | |_  \_|  | || |    | |       | || |   | |_  \_|  | |"
echo -e "$ANSI_COLOR_TS  (%%%%%%            °%%%%%%%%%%%%%%%%%%)  $ANSI_RESET     | |   |  _|  _   | || |    | |   _   | || |   |  _|      | |"
echo -e "$ANSI_COLOR_TS  (%%%%%%       y     °%%%%%%%%%%%%%%%%%)  $ANSI_RESET     | |  _| |___/ |  | || |   _| |__/ |  | || |  _| |_       | |"
echo -e "$ANSI_COLOR_TS  (%%%%%%       %b,      °%%%%%%%%%%%%%%)  $ANSI_RESET     | | |_________|  | || |  |________|  | || | |_____|      | |"
echo -e "$ANSI_COLOR_TS  (%%%%%%       %%%%b,           °%%%%%%)  $ANSI_RESET     | |              | || |              | || |              | |"
echo -e "$ANSI_COLOR_TS  (%%%%%%       %%%%%%%%%%b,         °%%)  $ANSI_RESET     | '--------------' || '--------------' || '--------------' |"
echo -e "$ANSI_COLOR_TS  (%%%%%%       %%%%%%%%%%%%%%%c,      °)  $ANSI_RESET      '----------------'  '----------------'  '----------------' "
echo -e "$ANSI_COLOR_TS  (%%%%%%       %%%%%%%%%%%%%%%%%,      °  $ANSI_RESET"
echo -e "$ANSI_COLOR_TS  (%%%%%%b      %%%%%%%%%%%%%%%%%%      ,  $ANSI_RESET                     EMBEDDED . LINUX . FACTORY"
echo -e "$ANSI_COLOR_TS  (%%%%%%%b     °%%%%%%%%%%%%%%%%°     ,e  $ANSI_RESET"
echo -e "$ANSI_COLOR_TS  (%%%%%%%%b,      °%%%%%%%%%%°       ,%)  $ANSI_RESET"
echo -e "$ANSI_COLOR_TS   °(%%%%%%%%%%b,                  ,%%)°   $ANSI_RESET"
echo
