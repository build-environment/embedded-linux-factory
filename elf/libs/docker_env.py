# -*- coding: utf-8 -*-

from __future__ import annotations

import logging
import re
import shlex
import stat
import subprocess
import sys
from pathlib import Path
from tempfile import TemporaryDirectory
from textwrap import indent
from typing import List, Optional, Union

from elf import DOCKER_RUN_PATH, PACKAGE_NAME

from .docker_volume import DockerVolume
from .script import Script


class DockerEnv:
    __logger = logging.getLogger(__name__)

    def __init__(
        self,
        image: str,
        docker_run_args: Optional[List[str]] = None,
    ) -> None:
        self.__image = image
        self.__run_args = docker_run_args if docker_run_args else []

    @property
    def image(self) -> str:
        return self.__image

    def check(self) -> bool:
        cmd = ["docker", "images", "--quiet", self.__image]
        process_result = subprocess.run(
            cmd,
            capture_output=True,
            check=False,
        )
        if process_result.returncode:
            if process_result.stdout:
                self.__logger.error(
                    "stdout:\n%s",
                    indent(process_result.stdout.decode().rstrip(), "> "),
                )
            if process_result.stderr:
                self.__logger.error(
                    "stderr:\n%s",
                    indent(process_result.stderr.decode().rstrip(), "> "),
                )
            # Raise exception
            process_result.check_returncode()

        if process_result.stdout.decode() == "":
            return False

        return True

    def pull_image(self) -> None:
        cmd = ["docker", "pull", self.__image]
        subprocess.run(
            cmd,
            check=True,
        )

    def with_env(self, env_var: str, value: str) -> DockerEnv:
        assert re.match(r"^\w+$", env_var), f"Invalid env_var: '{env_var}'"
        assert len(shlex.split(value)) <= 1, f"Invalid value: '{value}'"

        return self.with_args([f"--env={env_var}={value}"])

    def with_volume(self, volume: DockerVolume) -> DockerEnv:
        return self.with_args([volume.docker_run_arg])

    def with_args(self, docker_run_args: List[str]) -> DockerEnv:
        return DockerEnv(self.__image, self.__run_args + docker_run_args)

    @property
    def cmd(self) -> List[str]:
        return [
            sys.executable,
            str(DOCKER_RUN_PATH),
            "--image",
            self.__image,
        ] + self.__run_args

    def __str__(self) -> str:
        return " ".join(self.cmd)

    def __repr__(self) -> str:
        return f"{self.__class__.__qualname__}({str(self)})"

    def run(
        self,
        cmd: Union[str, List[str], None] = None,
        capture_output: bool = False,
        shell: bool = False,
        check: bool = False,
        timeout: Optional[float] = None,
    ) -> subprocess.CompletedProcess:
        command = [
            sys.executable,
            str(DOCKER_RUN_PATH),
            "--image",
            self.__image,
        ] + self.__run_args

        if cmd:
            if isinstance(cmd, str):
                cmd = [shlex.quote(cmd)]

            command += ["--cmd"] + cmd

        self.__logger.debug(" ".join(command))
        process_result = subprocess.run(
            command,
            capture_output=capture_output,
            shell=shell,
            check=False,
            timeout=timeout,
        )

        if process_result.stdout:
            self.__logger.debug(
                "stdout:\n%s",
                indent(process_result.stdout.decode().rstrip(), "> "),
            )
        if process_result.stderr:
            self.__logger.debug(
                "stderr:\n%s",
                indent(process_result.stderr.decode().rstrip(), "> "),
            )

        self.__logger.debug("$ %d", process_result.returncode)

        if check:
            process_result.check_returncode()

        return process_result

    def run_with_entrypoint(
        self,
        entry_point: Script,
        cmd: Union[str, List[str], None] = None,
        capture_output: bool = False,
        shell: bool = False,
        check: bool = False,
        timeout: Optional[float] = None,
    ) -> subprocess.CompletedProcess:
        with TemporaryDirectory(prefix=f"{PACKAGE_NAME}-") as tmp_dir:
            tmp_dir_path = Path(tmp_dir)
            docker_volume = DockerVolume(
                tmp_dir_path, f"/tmp/{tmp_dir_path.name}", "ro"
            )
            entrypoint_volume = docker_volume / "entrypoint.sh"
            entry_point.write(
                entrypoint_volume.path.open("w", encoding="utf-8")
            )
            entrypoint_volume.path.chmod(
                stat.S_IRWXU | stat.S_IRWXG | stat.S_IROTH
            )

            docker_env = self.with_volume(docker_volume).with_env(
                "ENTRYPOINT", entrypoint_volume.volume_path
            )

            return docker_env.run(
                cmd=cmd,
                capture_output=capture_output,
                shell=shell,
                check=check,
                timeout=timeout,
            )
