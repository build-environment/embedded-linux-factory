# -*- coding: utf-8 -*-

from __future__ import annotations

import posixpath
from pathlib import Path
from typing import Optional


class DockerVolume:
    def __init__(
        self, local_path: Path, volume_path: str, options: Optional[str] = None
    ) -> None:
        if not posixpath.isabs(volume_path):
            raise ValueError(f"volume_path='{volume_path}' shall be absolute")
        self.__local_path = local_path
        self.__docker_path = posixpath.normpath(volume_path)
        self.__volume_options = options

    @property
    def path(self) -> Path:
        return self.__local_path

    @property
    def volume_path(self) -> str:
        return self.__docker_path

    @property
    def docker_run_arg(self) -> str:
        return (
            f'--volume="{self.__local_path.absolute()}:{self.__docker_path}'
            f'{":" + self.__volume_options if self.__volume_options else ""}"'
        )

    def __repr__(self) -> str:
        return (
            f"{self.__class__.__qualname__}("
            f"{self.__local_path}:{self.__docker_path}"
            f"{':' + self.__volume_options if self.__volume_options else ''}"
            ")"
        )

    def joinpath(self, relative_path: str) -> DockerVolume:
        if not self.__local_path.is_dir():
            raise ValueError(
                f"{self.__local_path} is not a directory, "
                "cannot join a relative path"
            )
        if posixpath.isabs(relative_path):
            raise ValueError(
                f"{relative_path} is absolute, "
                f"cannot be joined to {self.__local_path}"
            )

        return DockerVolume(
            self.__local_path.joinpath(relative_path),
            f"{self.__docker_path}/{relative_path}",
            self.__volume_options,
        )

    def __truediv__(self, relative_path: str) -> DockerVolume:
        return self.joinpath(relative_path)
