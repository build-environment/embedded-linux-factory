# -*- coding: utf-8 -*-

import re
from typing import Any, Dict, List, Optional, cast

import click


class MatchingStringParamType(click.types.StringParamType):
    def __init__(
        self, name: str, pattern: re.Pattern, repr_str: Optional[str] = None
    ) -> None:
        self.name = name
        self.__pattern = pattern
        self.__repr = repr_str or f"{self.__class__.__qualname__}({name})"

    def convert(
        self,
        value: Any,
        param: Optional[click.Parameter] = None,
        ctx: Optional[click.Context] = None,
    ) -> Any:
        str_value = super().convert(value, param, ctx)

        if not self.__pattern.match(str_value):
            self.fail(
                f"'{str_value}' is not a valid {self.name}",
                ctx=ctx,
                param=param,
            )

        return str_value

    def __repr__(self) -> str:
        return self.__repr


class MappingChoice(click.Choice):
    def __init__(
        self, choices: Dict[str, Any], case_sensitive: bool = True
    ) -> None:
        self.__choices = choices
        choices_list = cast(List[str], choices.keys())
        super().__init__(choices_list, case_sensitive)

    def convert(
        self,
        value: Any,
        param: Optional[click.Parameter],
        ctx: Optional[click.Context],
    ) -> Any:
        return self.__choices[super().convert(value, param, ctx)]
