# -*- coding: utf-8 -*-

from __future__ import annotations

import os
from pathlib import Path
from typing import Any, Dict, Generator, Iterable, Optional, TextIO, Union

from jinja2 import Environment, PackageLoader

from elf import PACKAGE_NAME, PACKAGE_PATH, TEMPLATE_PATH


class Script:
    def __init__(self, lines: Optional[Iterable[str]] = None) -> None:
        self._lines = list(lines) if lines else []

    def __str__(self) -> str:
        return "\n".join(self._lines)

    def lines(self, with_endl: bool = False) -> Generator[str, None, None]:
        for line in self._lines:
            yield f"{line}\n" if with_endl else line

    def write(self, file: TextIO) -> None:
        for line in self.lines(True):
            file.write(line)


class CumulativeScript(Script):
    def __init__(self, shebang: str) -> None:
        assert len(shebang.splitlines()) == 1, "shebang shall be one-line"
        assert shebang.startswith("#!"), "shebang shall start with #!"

        super().__init__([shebang])

    def append(self, lines: Union[str, Iterable[str]]) -> None:
        if isinstance(lines, str):
            self._lines += lines.splitlines()
        else:
            self._lines += list(lines)

    def __iadd__(self, lines: Union[str, Iterable[str]]) -> CumulativeScript:
        self.append(lines)
        return self


class TemplatedScript(Script):
    def __init__(self, template: Path, context: Dict[str, Any]) -> None:
        assert template.is_file()

        env = Environment(
            loader=PackageLoader(
                package_name=PACKAGE_NAME,
                package_path=os.path.relpath(TEMPLATE_PATH, PACKAGE_PATH),
            )
        )

        template_script = env.get_template(
            os.path.relpath(template, TEMPLATE_PATH)
        )

        super().__init__(template_script.render(context).splitlines())
