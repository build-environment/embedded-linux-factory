# -*- coding: utf-8 -*-

from typing import Optional

import click


def click_command_parameter(
    command: click.Command, name: str
) -> Optional[click.Parameter]:
    return next(param for param in command.params if param.name == name)
