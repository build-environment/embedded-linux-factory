# -*- coding: utf-8 -*-

import logging
from typing import Iterable, Literal, Optional, Union

import click

LogLevel = int
LogLevelName = str


class ClickHandler(logging.Handler):
    _use_stderr = True

    def __init__(
        self, no_fmt_levels: Optional[Iterable[LogLevel]] = None
    ) -> None:
        self.__no_fmt_levels = list(no_fmt_levels) if no_fmt_levels else []

        super().__init__()

    def emit(self, record: logging.LogRecord) -> None:
        try:
            for msg in record.getMessage().splitlines():
                simple_line_record = logging.LogRecord(
                    name=record.name,
                    level=record.levelno,
                    pathname=record.pathname,
                    lineno=record.lineno,
                    msg=msg,
                    args=None,
                    exc_info=record.exc_info,
                    sinfo=record.stack_info,
                )

                if record.levelno in self.__no_fmt_levels:
                    click.echo(simple_line_record.getMessage())
                else:
                    click.echo(
                        self.format(simple_line_record),
                        err=self._use_stderr,
                    )

        except Exception:
            self.handleError(record)


def _normalize_logger(
    logger: Union[logging.Logger, str, None]
) -> logging.Logger:
    if not isinstance(logger, logging.Logger):
        logger = logging.getLogger(logger)

    return logger


def _colored_log_fmt(
    fmt: Optional[str], fmt_style: Literal["%", "{", "$"]
) -> Union[str, None]:
    colors_formats = {
        "%": "%(log_color)s",
        "{": "{log_color}",
        "$": "${log_color}",
    }

    if fmt is None:
        return None

    return f"{colors_formats[fmt_style]}{fmt}"


def basic_config(
    logger: Union[logging.Logger, str, None] = None,
    log_level: Union[LogLevel, LogLevelName] = logging.INFO,
    colors: bool = True,
    fmt: Optional[str] = None,
    fmt_style: Literal["%", "{", "$"] = "%",
    no_fmt_levels: Optional[Iterable[LogLevel]] = None,
):
    logger = _normalize_logger(logger)

    log_handler = ClickHandler(no_fmt_levels)
    if colors:
        from colorlog import ColoredFormatter

        log_colors = {
            "DEBUG": "light_black",
            "INFO": "white",
            "WARNING": "yellow",
            "ERROR": "red",
            "CRITICAL": "light_red",
        }

        log_handler.formatter = ColoredFormatter(
            fmt=_colored_log_fmt(fmt, fmt_style),
            style=fmt_style,
            log_colors=log_colors,
        )
    else:
        log_handler.formatter = logging.Formatter(
            fmt=fmt,
            style=fmt_style,
        )

    logger.setLevel(log_level)
    logger.handlers = [log_handler]
    logger.propagate = False

    return logger
