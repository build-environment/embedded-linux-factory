# -*- coding: utf-8 -*-

from pathlib import Path

__all__ = [
    "COMMANDS_PACKAGE_NAME",
    "COMMANDS_PATH",
    "DOCKER_IMAGE",
    "DOCKER_RUN_PATH",
    "ENTRYPOINT_TEMPLATE_PATH",
    "PACKAGE_NAME",
    "PACKAGE_PATH",
    "SCRIPTS_PATH",
    "TEMPLATE_PATH",
    "TOOLS_PATH",
]

DOCKER_IMAGE = "registry.gitlab.com/build-environment/yocto-env:latest"
PACKAGE_NAME = __name__
PACKAGE_PATH = Path(__file__).parent
COMMANDS_PATH = PACKAGE_PATH / "commands"
COMMANDS_PACKAGE_NAME = f"{PACKAGE_NAME}.{COMMANDS_PATH.name}"
TEMPLATE_PATH = PACKAGE_PATH / "templates"
TOOLS_PATH = PACKAGE_PATH / "tools"
ENTRYPOINT_TEMPLATE_PATH = TEMPLATE_PATH / "elf_entrypoint.sh.j2"
SCRIPTS_PATH = PACKAGE_PATH / "scripts"
DOCKER_RUN_PATH = SCRIPTS_PATH / "docker_run.py"
