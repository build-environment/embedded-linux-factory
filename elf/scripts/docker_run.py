#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import getpass
import os
import platform
import re
import subprocess
import sys
from argparse import (
    Action,
    ArgumentParser,
    ArgumentTypeError,
    Namespace,
    RawDescriptionHelpFormatter,
)
from dataclasses import dataclass
from pathlib import Path
from typing import Any, Dict, List, Literal, Optional, Sequence, Union


@dataclass
class DotFile:
    file_name: str
    mount_opt: Optional[str] = None


DOT_FILES_MOUNT: List[DotFile] = [
    DotFile(".bash_history"),
    DotFile(".gitconfig", "ro"),
    DotFile(".inputrc", "ro"),
    DotFile(".ssh", "ro"),
    DotFile(".netrc", "ro"),
]


def existing_dir_path(value: str):
    path = Path(value)
    if path.exists() and path.is_dir():
        return path
    else:
        raise ArgumentTypeError(
            f'"{path.absolute()}" is not an existing directory path.'
        )


class MultipleArgumentAction(Action):
    def __init__(
        self,
        option_strings: Sequence[str],
        dest: str,
        values: Dict[str, Any],
        nargs: Union[int, str, None] = None,
        **kwargs,
    ) -> None:
        self.values = values
        super().__init__(option_strings, dest, nargs, **kwargs)

    def __call__(
        self,
        parser: ArgumentParser,
        namespace: Namespace,
        values: Union[str, Sequence[Any], None],
        option_string: Union[str, None] = None,
    ) -> None:
        for dest, default_value in self.values.items():
            setattr(
                namespace, dest, default_value if values is None else values
            )


class OptionalValueAction(Action):
    def __init__(
        self,
        option_strings: Sequence[str],
        dest: str,
        default_value: Any,
        nargs: Union[int, Literal["*", "+", "?", "..."], None] = None,
        **kwargs,
    ) -> None:
        if nargs not in ["?", "*"]:
            raise ValueError(f"Invalid nargs value: {nargs}")

        self.default_value = default_value

        super().__init__(option_strings, dest, nargs, **kwargs)

    def __call__(
        self,
        parser: ArgumentParser,
        namespace: Namespace,
        values: Union[str, Sequence[Any], None],
        option_string: Union[str, None] = None,
    ) -> None:
        setattr(
            namespace,
            self.dest,
            self.default_value if values is None else values,
        )


def volume_cmd(
    local_path: Path, container_path: str, opt: Optional[str] = None
) -> List[str]:
    cmd = (
        [
            "--volume",
            f"{local_path.absolute()}:{container_path}"
            f'{f":{opt}" if opt else ""}',
        ]
        if local_path.exists()
        else []
    )

    return cmd


def mount_dot_file_cmd(file: DotFile, home_path: str) -> List[str]:
    return volume_cmd(
        Path.home().joinpath(file.file_name),
        f"{home_path}/{file.file_name}",
        file.mount_opt,
    )


def cmd_line(namespace: Namespace, additional_args: List[str]) -> List[str]:
    docker_image_pattern = re.compile(
        r"([-\w\.\/]+/)?(?P<image_name>[^:/]+)(:.+)?$"
    )
    username = getpass.getuser()
    docker_image_groups = docker_image_pattern.search(namespace.docker_image)
    assert docker_image_groups is not None
    container_hostname = (
        f"{docker_image_groups['image_name']}.{platform.node()}"
    )

    cmd = ["docker", "run", "--interactive", "--tty"]

    if namespace.persistent is False:
        cmd += ["--rm"]

    if sys.platform == "win32":
        container_workdir = "/workdir"
    else:
        container_workdir = namespace.workdir
        cmd += [
            "--env",
            f"MP_UID={os.getuid()}",
            "--env",
            f"MP_GID={os.getgid()}",
        ]

    if namespace.http_proxy_url:
        cmd += ["--env", f"HTTP_PROXY={namespace.http_proxy_url}"]
    if namespace.https_proxy_url:
        cmd += ["--env", f"HTTPS_PROXY={namespace.https_proxy_url}"]

    cmd += [
        "--env",
        f"MP_USER={username}",
        "--volume",
        f"{namespace.workdir}:{container_workdir}",
        "--workdir",
        f"{namespace.cd_path or container_workdir}",
        "--label",
        f'"user={username},workdir={namespace.workdir}"',
        "--hostname",
        f"{container_hostname}",
    ]

    if namespace.mount_dot_files:
        for dot_file in DOT_FILES_MOUNT:
            cmd += mount_dot_file_cmd(dot_file, f"/home/{username}")

    cmd += additional_args

    cmd += [namespace.docker_image]

    if namespace.cmd:
        cmd += namespace.cmd

    return cmd


def run_interactive(cmd: Sequence[str]) -> int:
    return subprocess.run(
        " ".join(cmd),
        shell=True,
        check=False,
    ).returncode


def main():
    parser = ArgumentParser(
        description="Create and run a new container from an image.",
        formatter_class=RawDescriptionHelpFormatter,
        epilog='The additional arguments are forwarded to "docker run",\n'
        'see "docker run --help" for more details.',
    )
    parser.add_argument(
        "-I",
        "--image",
        metavar="image",
        dest="docker_image",
        required=True,
        help="Docker image",
    )
    parser.add_argument(
        "-w",
        "--workdir",
        metavar="workdir_path",
        dest="workdir",
        default=Path.cwd(),
        type=existing_dir_path,
        help="Bind mount the workdir as a volume",
    )
    parser.add_argument(
        "-D",
        "--cd",
        metavar="path",
        dest="cd_path",
        help="Working directory inside the container",
    )
    parser.add_argument(
        "--no-rm",
        "-R",
        dest="persistent",
        action="store_true",
        help="Start a persistent docker container",
    )
    parser.add_argument(
        "--no-dot-files",
        "-n",
        dest="mount_dot_files",
        action="store_false",
        help="Do not mount user dot files from home",
    )
    parser.add_argument(
        "--http-proxy",
        nargs="?",
        metavar="proxy_url",
        dest="http_proxy_url",
        action=OptionalValueAction,
        default_value=os.environ.get("HTTP_PROXY", None),
        help="Set the http proxy, if proxy_url is not set, "
        "uses the HTTP_PROXY environment variable",
    )
    parser.add_argument(
        "--https-proxy",
        nargs="?",
        metavar="proxy_url",
        dest="https_proxy_url",
        action=OptionalValueAction,
        default_value=os.environ.get("HTTPS_PROXY", None),
        help="Set the http proxy, if proxy_url is not set, "
        "uses the HTTPS_PROXY environment variable",
    )
    parser.add_argument(
        "--proxy",
        nargs="?",
        metavar="proxy_url",
        action=MultipleArgumentAction,
        values={
            "http_proxy_url": os.environ.get("HTTP_PROXY", None),
            "https_proxy_url": os.environ.get("HTTPS_PROXY", None),
        },
        help="Set the proxy, if proxy_url is not set, "
        "uses the HTTP_PROXY and HTTPS_PROXY environment variable",
    )
    parser.add_argument(
        "-c",
        "--cmd",
        dest="cmd",
        nargs="...",
        help="Command line executed inside the docker container",
    )

    namespace, args = parser.parse_known_args()
    cmd = cmd_line(namespace, args)
    sys.exit(run_interactive(cmd))


if __name__ == "__main__":
    main()
