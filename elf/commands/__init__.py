# -*- coding: utf-8 -*-

"""
This is an example how-to add a new click command to ELF.

`main.py` will import each module inside 'commands' folder
and add `command()` to the commands list.

If needed, a `complete_args()` can be defined to update
dynamically the arguments definition.


    >>> import click
    >>>
    >>> from ..libs.click_tools import click_command_parameter
    >>>
    >>>
    >>> @click.command(
    >>>     name="command_name",
    >>> )
    >>> @click.option(
    >>>     "--my-option",
    >>>     type=click.STRING,
    >>> )
    >>> def command(my_option: str) -> None:
    >>>     click.echo(f"my_option: {my_option}")
    >>>
    >>>
    >>> def complete_args(ctx: click.Context) -> None:
    >>>     option = click_command_parameter(command, "my_option")
    >>>     assert option
    >>>
    >>>     option.type = click.Choice(["OPTION_1", "OPTION_2"])
"""
