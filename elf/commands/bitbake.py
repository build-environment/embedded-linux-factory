# -*- coding: utf-8 -*-

from typing import Optional

import click

from ..elf import Elf
from ..libs.click_tools import click_command_parameter
from .run import elf_entrypoint


@click.command(
    name="bitbake",
    help="Run bitbake commands inside the docker container.",
    options_metavar="[OPTIONS] -- [ARG...]",
    context_settings={
        "ignore_unknown_options": True,
        "allow_extra_args": True,
    },
)
@click.option(
    "--config",
    help="Select a config form Manifest.",
    type=click.STRING,
)
@click.help_option("--help", "-h")
@click.pass_obj
@click.pass_context
def command(ctx: click.Context, obj: Elf, config: Optional[str]):
    entry_point = elf_entrypoint(config)
    cmd = ["bitbake"] + ctx.args

    ctx.exit(
        obj.docker_env.run_with_entrypoint(
            entry_point=entry_point,
            cmd=cmd,
        ).returncode
    )


def complete_args(ctx: click.Context) -> None:
    config_option = click_command_parameter(command, "config")
    assert config_option

    config_option.type = click.Choice(
        list(ctx.obj.manifest_iter("config", "name"))
    )
