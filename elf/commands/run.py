# -*- coding: utf-8 -*-

from typing import Optional

import click

from .. import ENTRYPOINT_TEMPLATE_PATH, TOOLS_PATH
from ..elf import Elf
from ..libs.click_tools import click_command_parameter
from ..libs.docker_volume import DockerVolume
from ..libs.script import Script, TemplatedScript


def elf_entrypoint(config_name: Optional[str], verbose: bool) -> Script:
    tools_volume = DockerVolume(TOOLS_PATH, "/opt/elf-tools", "ro")

    return TemplatedScript(
        template=ENTRYPOINT_TEMPLATE_PATH,
        context={
            "manifest_script_path": tools_volume.joinpath(
                "manifest.py"
            ).volume_path,
            "yocto_config": config_name,
            "verbose": verbose,
        },
    )


@click.command(
    name="run",
    help="Run the docker container.",
    options_metavar="[OPTIONS] -- [COMMAND] [ARG...]",
    context_settings={
        "ignore_unknown_options": True,
        "allow_extra_args": True,
    },
)
@click.option(
    "--config",
    help="Select a config form Manifest.",
    type=click.STRING,
)
@click.help_option("--help", "-h")
@click.pass_obj
@click.pass_context
def command(ctx: click.Context, obj: Elf, config: Optional[str]):
    entrypoint = elf_entrypoint(config, obj.verbose)

    ctx.exit(
        obj.docker_env.run_with_entrypoint(
            entry_point=entrypoint,
            cmd=ctx.args,
        ).returncode
    )


def complete_args(ctx: click.Context) -> None:
    config_option = click_command_parameter(command, "config")
    assert config_option

    config_option.type = click.Choice(
        list(ctx.obj.manifest_iter("config", "name"))
    )
