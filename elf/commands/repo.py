# -*- coding: utf-8 -*-

import click

from ..elf import Elf


@click.command(
    name="repo",
    short_help="Run repo commands inside the docker container.",
    add_help_option=False,
    context_settings={
        "ignore_unknown_options": True,
        "allow_extra_args": True,
    },
)
@click.pass_obj
@click.pass_context
def command(ctx: click.Context, obj: Elf):
    cmd = ["repo"] + ctx.args
    obj.logger.debug(" ".join(cmd))

    result = obj.docker_env.run(cmd, check=False).returncode

    ctx.exit(result)
