
WORKSPACE_PATH := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
VIRTUAL_ENV := $(WORKSPACE_PATH)/venv
ACTIVATE_VENV := $(VIRTUAL_ENV)/bin/activate

setup-pre-commit:
	@$(WORKSPACE_PATH)/setup-pre-commit.sh

################################################
venv:
	@virtualenv $(VIRTUAL_ENV)

$(VIRTUAL_ENV): venv

clean-venv:
	@rm -rf $(VIRTUAL_ENV)

.in-venv: $(VIRTUAL_ENV)
	@( \
		echo $$PATH | grep "$(VIRTUAL_ENV)/bin" >/dev/null ;\
		if [ $$? != 0 ]; then \
			echo "Please run the following command:" ;\
			echo ;\
			echo "source $(VIRTUAL_ENV)/bin/activate" ;\
			echo ;\
			exit 1 ;\
		fi \
	)

################################################
install:
	@pip install $(WORKSPACE_PATH)

uninstall:
	@pip uninstall -y elf

develop: .in-venv
	@pip install --editable $(WORKSPACE_PATH)

.PHONY : setup-pre-commit venv clean-venv install uninstall develop
.DEFAULT_GOAL := all
