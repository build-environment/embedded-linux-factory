# Embedded Linux Factory

[![license](https://img.shields.io/gitlab/license/build-environment/embedded-linux-factory)](https://gitlab.com/build-environment/embedded-linux-factory)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

## Description

This tool is a prototype that aims to be an alternative to
[ELF](https://gitlab.com/embedded_linux_factory/elf) project.

It is based on the [yocto-env](https://gitlab.com/build-environment/yocto-env)
project, that provides a `yocto-env` docker image
which contains `git-repo` and the Yocto build toolchain.

This tool allows you to launch a Docker container that provides all the tools
required to develop and build a Yocto project based on a git-repo manifest file.

## Setup

### Install ELF

```bash
$ cd ~/workspace/
$ git clone https://gitlab.com/build-environment/embedded-linux-factory.git
Cloning into 'embedded-linux-factory'...
remote: Enumerating objects: 92, done.
remote: Counting objects: 100% (92/92), done.
remote: Compressing objects: 100% (87/87), done.
remote: Total 92 (delta 41), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (92/92), 25.94 KiB | 12.97 MiB/s, done.
Resolving deltas: 100% (41/41), done.
$ cd ~/workspace/embedded-linux-factory
$ pip install .
```

## Usage

### Manifest Example

The manifest file used for the Yocto project extends the
[git-repo manifest](https://github.com/GerritCodeReview/git-repo/blob/main/docs/manifest-format.md)
format. This allow to use [git-repo](https://github.com/GerritCodeReview/git-repo)
tool as versioning tool.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
  <default sync-j="4" revision="kirkstone"/>

  <!-- Yocto -->
  <remote fetch="https://git.yoctoproject.org/git" name="yocto"/>
  <project remote="yocto" name="poky" path="layers/poky"/>

  <!-- raspberry Pi -->
  <remote fetch="https://github.com/agherzan" name="raspberrypi"/>
  <project remote="raspberrypi" name="meta-raspberrypi" path="layers/meta-raspberrypi"/>

  <!-- openembedded -->
  <remote fetch="https://github.com/openembedded" name="oe"/>
  <project remote="oe" name="meta-openembedded" path="layers/meta-openembedded"/>
  <layer path="layers/meta-openembedded/meta-oe"/>
  <layer path="layers/meta-openembedded/meta-python"/>

  <!-- Config -->
  <remote fetch="https://my.git-server.com" name="my-server"/>
  <project remote="my-server" name="yocto-config" path="config" revision="master"/>

  <!-- Config definition for raspberrypi3 -->
  <config name="raspberrypi3">
    <!-- Add a specific layer for this configuration -->
    <layer path="layers/meta-raspberrypi"/>

    <!--
      Use an alternative template for bitbake configuration files
      the path will be as for TEMPLATECONF to init the build environment
    -->
    <template-conf path="config/rpi3"/>

    <!--
      Set environment variable to <build_dir>/conf/project.conf
      project.conf will be included to <build_dir>/conf/local.conf
    -->
    <project-var ENABLE_UART="1"/>
  </config>

  <!-- Config definition for qemux86-64 -->
  <config name="qemux86-64">
    <!-- Set an environment variable inside docker container -->
    <env-var SDKMACHINE="i686"/>
  </config>

</manifest>
```

### Setup a project

* Create a git repository hosting the manifest file.
* Add a new manifest file named `default.xml` based on the above example.
* Create a directory where will be located your project.

```bash
mkdir -p ~/workspace/my-project
cd ~/workspace/my-project
```

### Using ELF

#### Initialize your project: elf repo init

```bash
$ elf repo init -u https://my.git-server.com/my-project.git -b master
# repo-init

repo has been initialized in ~/workspace/my-project
```

#### Checkout your project: elf repo sync

```bash
$ elf repo sync
# repo-sync
Fetching: 100% (7/7), done in 105.039s
repo sync has finished successfully.
```

#### Enter to build environment

```bash
$ elf run --config raspberrypi3

   ,(%%%%%%%P°  %%%%%%%%%%%%%%%%%%%%%%),
  (%%%%%%°      %%%%%%%%%%%%%%%%%%%%%%%%)
  (%%%%%%       %%%%%%%%%%%%%%%%%%%%j%%%)
                                     "²0)
                                      ,%)        .----------------.  .----------------.  .----------------.
  (%%%%%%%%%%%%%%%%%%%%%%%%%%%%bo,_  ,%%)       | .--------------. || .--------------. || .--------------. |
  (%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%,%%%)       | |  _________   | || |   _____      | || |  _________   | |
  (%%%%%%            ,%%%%%%%%%%%%%%%%%%)       | | |_   ___  |  | || |  |_   _|     | || | |_   ___  |  | |
  (%%%%%%            %%%%%%%%%%%%%%%%%%%)       | |   | |_  \_|  | || |    | |       | || |   | |_  \_|  | |
  (%%%%%%            °%%%%%%%%%%%%%%%%%%)       | |   |  _|  _   | || |    | |   _   | || |   |  _|      | |
  (%%%%%%       y     °%%%%%%%%%%%%%%%%%)       | |  _| |___/ |  | || |   _| |__/ |  | || |  _| |_       | |
  (%%%%%%       %b,      °%%%%%%%%%%%%%%)       | | |_________|  | || |  |________|  | || | |_____|      | |
  (%%%%%%       %%%%b,           °%%%%%%)       | |              | || |              | || |              | |
  (%%%%%%       %%%%%%%%%%b,         °%%)       | '--------------' || '--------------' || '--------------' |
  (%%%%%%       %%%%%%%%%%%%%%%c,      °)        '----------------'  '----------------'  '----------------'
  (%%%%%%       %%%%%%%%%%%%%%%%%,      °
  (%%%%%%b      %%%%%%%%%%%%%%%%%%      ,                       EMBEDDED . LINUX . FACTORY
  (%%%%%%%b     °%%%%%%%%%%%%%%%%°     ,e
  (%%%%%%%%b,      °%%%%%%%%%%°       ,%)
   °(%%%%%%%%%%b,                  ,%%)°



You had no conf/local.conf file. This configuration file has therefore been
created for you from ~/workspace/my-project/layers/poky/meta-poky/conf/local.conf.sample
You may wish to edit it to, for example, select a different MACHINE (target
hardware). See conf/local.conf for more information as common configuration
options are commented.

You had no conf/bblayers.conf file. This configuration file has therefore been
created for you from ~/workspace/my-project/layers/poky/meta-poky/conf/bblayers.conf.sample
To add additional metadata layers into your configuration please add entries
to conf/bblayers.conf.

The Yocto Project has extensive documentation about OE including a reference
manual which can be found at:
    https://docs.yoctoproject.org

For more information about OpenEmbedded see the website:
    https://www.openembedded.org/


### Shell environment set up for builds. ###

You can now run 'bitbake <target>'

Common targets are:
    core-image-minimal
    core-image-full-cmdline
    core-image-sato
    core-image-weston
    meta-toolchain
    meta-ide-support

You can also run generated qemu images with a command like 'runqemu qemux86'

Other commonly useful commands are:
 - 'devtool' and 'recipetool' handle common recipe tasks
 - 'bitbake-layers' handles common layer tasks
 - 'oe-pkgdata-util' handles common target package tasks
NOTE: Starting bitbake server...
user@yocto-env:~/workspace/my-project/build/raspberrypi3$
```

#### Have fun

You shall now be able to build your Yocto project.

## How-to contribute

### Setup ELF development environment

To develop and debug the project, you shall setup a correct virtual environment
and install ELF locally. Ths can be done by sourcing `setup-dev-env`.

```bash
$ cd ~/workspace/embedded-linux-factory
$ source ./setup-dev-env
created virtual environment CPython3.8.10.final.0-64 in 171ms
  creator CPython3Posix(dest=~/workspace/embedded-linux-factory/venv, clear=False, no_vcs_ignore=False, global=False)
  seeder FromAppData(download=False, pip=bundle, setuptools=bundle, wheel=bundle, via=copy, app_data_dir=~/.local/share/virtualenv)
    added seed packages: pip==23.1, setuptools==67.6.1, wheel==0.40.0
  activators BashActivator,CShellActivator,FishActivator,NushellActivator,PowerShellActivator,PythonActivator
Obtaining file:///home/user/workspace/embedded-linux-factory
  Installing build dependencies ... done
  Checking if build backend supports build_editable ... done
  Getting requirements to build editable ... done
  Preparing editable metadata (pyproject.toml) ... done
Requirement already satisfied: Click>=8.1 in ./venv/lib/python3.8/site-packages (from elf==0.0.1) (8.1.3)
Requirement already satisfied: colorlog>=6.7 in ./venv/lib/python3.8/site-packages (from elf==0.0.1) (6.7.0)
Requirement already satisfied: mypy>=1.2 in ./venv/lib/python3.8/site-packages (from elf==0.0.1) (1.2.0)
Requirement already satisfied: jinja2>=3.1 in ./venv/lib/python3.8/site-packages (from elf==0.0.1) (3.1.2)
Requirement already satisfied: lxml>=4.9 in ./venv/lib/python3.8/site-packages (from elf==0.0.1) (4.9.2)
Requirement already satisfied: MarkupSafe>=2.0 in ./venv/lib/python3.8/site-packages (from jinja2>=3.1->elf==0.0.1) (2.1.2)
Requirement already satisfied: typing-extensions>=3.10 in ./venv/lib/python3.8/site-packages (from mypy>=1.2->elf==0.0.1) (4.5.0)
Requirement already satisfied: mypy-extensions>=1.0.0 in ./venv/lib/python3.8/site-packages (from mypy>=1.2->elf==0.0.1) (1.0.0)
Requirement already satisfied: tomli>=1.1.0 in ./venv/lib/python3.8/site-packages (from mypy>=1.2->elf==0.0.1) (2.0.1)
Building wheels for collected packages: elf
  Building editable for elf (pyproject.toml) ... done
  Created wheel for elf: filename=elf-0.0.1-0.editable-py3-none-any.whl size=3354 sha256=0ab0549fc9d8013e31606671690f64a3827df42c52f7d4ea8f59b2693aed6dd6
  Stored in directory: /tmp/pip-ephem-wheel-cache-prd62iyj/wheels/d7/86/bc/9bb96937b4f6c0476cb0272ca56828b1c4cefd690b2785ecc2
Successfully built elf
Installing collected packages: elf
  Attempting uninstall: elf
    Found existing installation: elf 0.0.1
    Uninstalling elf-0.0.1:
      Successfully uninstalled elf-0.0.1
Successfully installed elf-0.0.1
```

### Setup pre-commit hooks

To contribute to this project, please ensure that the following tools are installed:

* gitlint
* pre-commit
* shfmt
* shellcheck

You can automatically check and install the tools using the following command:

```shell
$ ./setup-pre-commit.sh
Start of dev environment setup
Checking tools...
* pip... OK
* shfmt... OK
* shellcheck... Installed
* pre-commit... OK
* gitlint... OK

Checking hooks...
* pre-commit... Installed
* commit-msg... Installed

Dev environment is ready!
```
